/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package component;

import com.nawapat.dcoffeeproject.model.ProductModel;

/**
 *
 * @author Gxz32
 */
public interface BuyProductable {
    //    public void buy(ProductModel product, int qty);
    public void buy(ProductModel product, String productSize, String productSweetLevel, int qty);
}
