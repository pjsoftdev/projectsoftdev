/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kornn
 */
public class CustomerModel {

    private int id;
    private String key;
    private String name;
    private String phone;
    private int point;
    private String search;

    public CustomerModel(int id, String key, String name, String phone, int point, String search) {
        this.id = id;
        this.key = key;
        this.name = name;
        this.phone = phone;
        this.point = point;
        this.search = search;
    }

    public CustomerModel(String key, String name, String phone, int point, String search) {
        this.id = -1;
        this.key = key;
        this.name = name;
        this.phone = phone;
        this.point = point;
        this.search = search;
    }

    public CustomerModel() {
        this.id = -1;
        this.key = "";
        this.name = "";
        this.phone = "";
        this.point = 0;
        this.search = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    @Override
    public String toString() {
        return "CustomerModel{" + "id=" + id + ", key=" + key + ", name=" + name + ", phone=" + phone + ", point=" + point + '}';
    }
   
    public static CustomerModel fromRS(ResultSet rs) {
        CustomerModel customer = new CustomerModel();
        try {
            customer.setId(rs.getInt("customer_id"));
            customer.setKey(rs.getString("customer_key"));
            customer.setName(rs.getString("customer_name"));
            customer.setPhone(rs.getString("customer_phone"));
            customer.setPoint(rs.getInt("customer_point"));
            customer.setSearch(rs.getString("customer_phone"));
        } catch (SQLException ex) {
            Logger.getLogger(CustomerModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return customer;
    }
}
