    /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.model;

import com.nawapat.dcoffeeproject.dao.PaymentDao;
import com.nawapat.dcoffeeproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kornn
 */
public class CheckInCheckOutModel {

    private int id;
    private Date checkin;
    private Date checkout;
    private int duration;
    private int status;
    private int paymentid;
    private int userid;
    private UserModel user;
    private PaymentModel payment;

    public CheckInCheckOutModel(int id, Date checkin, Date checkout, int duration, int status, int paymentid, int userid) {
        this.id = id;
        this.checkin = checkin;
        this.checkout = checkout;
        this.duration = duration;
        this.status = status;
        this.paymentid = paymentid;
        this.userid = userid;
    }
    
    public CheckInCheckOutModel(Date checkin, Date checkout, int duration, int status, int paymentid, int userid) {
        this.id = -1;
        this.checkin = checkin;
        this.checkout = checkout;
        this.duration = duration;
        this.status = status;
        this.paymentid = paymentid;
        this.userid = userid;
    }
    
    public CheckInCheckOutModel() {
        this.id = -1;
        this.checkin = null;
        this.checkout = null;
        this.duration = 0;
        this.status = 0;
        this.paymentid = 0;
        this.userid = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCheckin() {
        return checkin;
    }

    public void setCheckin(Date checkin) {
        this.checkin = checkin;
    }

    public Date getCheckout() {
        return checkout;
    }

    public void setCheckout(Date checkout) {
        this.checkout = checkout;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPaymentid() {
        return paymentid;
    }

    public void setPaymentid(int paymentid) {
        this.paymentid = paymentid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }
    
    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
        this.userid = user.getId();
    }

    public PaymentModel getPayment() {
        return payment;
    }

    public void setPayment(PaymentModel payment) {
        this.payment = payment;
    }
    
    

    @Override
    public String toString() {
        return "CheckInCheckOutModel{" + "id=" + id + ", checkin=" + checkin + ", checkout=" + checkout + ", duration=" + duration + ", status=" + status + ", paymentid=" + paymentid + ", userid=" + userid + '}';
    }
    
    public static CheckInCheckOutModel fromRS(ResultSet rs) {
        CheckInCheckOutModel checkincheckout = new CheckInCheckOutModel();
        try {
            checkincheckout.setId(rs.getInt("id"));
            checkincheckout.setCheckin(rs.getTimestamp("checkin"));
            checkincheckout.setCheckout(rs.getTimestamp("checkout"));
            checkincheckout.setDuration(rs.getInt("duration"));
            checkincheckout.setStatus(rs.getInt("status"));
            checkincheckout.setPaymentid(rs.getInt("payment_id"));
            checkincheckout.setUserid(rs.getInt("user_id"));
            
            UserDao userDao = new UserDao();
            UserModel user = userDao.get(checkincheckout.getUserid());
            checkincheckout.setUser(user);
            
            PaymentDao paymentDao = new PaymentDao();
            PaymentModel payment = paymentDao.get(checkincheckout.getPaymentid());
            checkincheckout.setPayment(payment);
        } catch (SQLException ex) {
            Logger.getLogger(CheckInCheckOutModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkincheckout;
    }
}
