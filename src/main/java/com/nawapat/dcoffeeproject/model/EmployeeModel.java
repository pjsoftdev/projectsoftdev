/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kornn
 */
public class EmployeeModel {

    private int id;
    private String key;
    private String name;
    private String phone;
    private int position;
    private float hoursalary;

    public EmployeeModel(int id, String key, String name, String phone, int position, float hoursalary) {
        this.id = id;
        this.key = key;
        this.name = name;
        this.phone = phone;
        this.position = position;
        this.hoursalary = hoursalary;
    }
    
    public EmployeeModel(String key, String name, String phone, int position, float hoursalary) {
        this.id = -1;
        this.key = key;
        this.name = name;
        this.phone = phone;
        this.position = position;
        this.hoursalary = hoursalary;
    }

    public EmployeeModel() {
        this.id = -1;
        this.key = "";
        this.name = "";
        this.phone = "";
        this.position = 0;
        this.hoursalary = 0;
    }

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public float getHoursalary() {
        return hoursalary;
    }

    public void setHoursalary(float hoursalary) {
        this.hoursalary = hoursalary;
    }

    @Override
    public String toString() {
        return "EmployeeModel{" + "id=" + id + ", key=" + key + ", name=" + name + ", phone=" + phone + ", position=" + position + ", hoursalary=" + hoursalary + '}';
    }
    
    


    public static EmployeeModel fromRS(ResultSet rs) {
        EmployeeModel employee = new EmployeeModel();
        try {
            employee.setId(rs.getInt("employee_id"));
            employee.setKey(rs.getString("employee_key"));
            employee.setName(rs.getString("employee_name"));
            employee.setPhone(rs.getString("employee_phone"));
            employee.setPosition(rs.getInt("employee_position"));
            employee.setHoursalary(rs.getFloat("employee_hourysalary"));
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }
}
