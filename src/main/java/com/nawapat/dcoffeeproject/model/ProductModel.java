/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gxz32
 */
public class ProductModel {
    private int id;
    private String key;
    private String name;
    private int price;
    private String size;
    private String sweetLevel;
    private int category;

    public ProductModel(int id, String key, String name, int price, String size, String sweetLevel, int category) {
        this.id = id;
        this.key = key;
        this.name = name;
        this.price = price;
        this.size = size;
        this.sweetLevel = sweetLevel;
        this.category = category;
    }
    
    public ProductModel(String key, String name, int price, String size, String sweetLevel, int category) {
        this.id = -1;
        this.key = key;
        this.name = name;
        this.price = price;
        this.size = size;
        this.sweetLevel = sweetLevel;
        this.category = category;
    }
    
    public ProductModel() {
        this.id = -1;
        this.key = "";
        this.name = "";
        this.price = 0;
        this.size = "";
        this.sweetLevel = "";
        this.category = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSweetLevel() {
        return sweetLevel;
    }

    public void setSweetLevel(String sweetLevel) {
        this.sweetLevel = sweetLevel;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "ProductModel{" + "id=" + id + ", key=" + key + ", name=" + name + ", price=" + price + ", size=" + size + ", sweetLevel=" + sweetLevel + ", category=" + category + '}';
    }
    
    public static ProductModel fromRS(ResultSet rs) {
        ProductModel product = new ProductModel();
        try {
            product.setId(rs.getInt("product_id"));
            product.setKey(rs.getString("product_key"));
            product.setName(rs.getString("product_name"));
            product.setPrice(rs.getInt("product_price"));
            product.setSize(rs.getString("product_size"));
            product.setSweetLevel(rs.getString("product_sweet_level"));
            product.setCategory(rs.getInt("product_category"));
        } catch (SQLException ex) {
            Logger.getLogger(ProductModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }
}
