/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gxz32
 */
public class HistoryMatDetailModel {

    private int id;
    private int materialId;
    private String materialKey;
    private String materialName;
    private String materialUnit;
    private int materialRemain;
    private int historyMaterialId;

    public HistoryMatDetailModel(int id, int materialId, String materialKey, String materialName, String materialUnit, int materialRemain, int historyMaterialId) {
        this.id = id;
        this.materialId = materialId;
        this.materialKey = materialKey;
        this.materialName = materialName;
        this.materialUnit = materialUnit;
        this.materialRemain = materialRemain;
        this.historyMaterialId = historyMaterialId;
    }

    public HistoryMatDetailModel(int materialId, String materialKey, String materialName, String materialUnit, int materialRemain, int historyMaterialId) {
        this.id = -1;
        this.materialId = materialId;
        this.materialKey = materialKey;
        this.materialName = materialName;
        this.materialUnit = materialUnit;
        this.materialRemain = materialRemain;
        this.historyMaterialId = historyMaterialId;
    }
    
        public HistoryMatDetailModel() {
        this.id = -1;
        this.materialId = 0;
        this.materialKey = "";
        this.materialName = "";
        this.materialUnit = "";
        this.materialRemain = 0;
        this.historyMaterialId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public String getMaterialKey() {
        return materialKey;
    }

    public void setMaterialKey(String materialKey) {
        this.materialKey = materialKey;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getMaterialUnit() {
        return materialUnit;
    }

    public void setMaterialUnit(String materialUnit) {
        this.materialUnit = materialUnit;
    }

    public int getMaterialRemain() {
        return materialRemain;
    }

    public void setMaterialRemain(int materialRemain) {
        this.materialRemain = materialRemain;
    }

    public int getHistoryMaterialId() {
        return historyMaterialId;
    }

    public void setHistoryMaterialId(int historyMaterialId) {
        this.historyMaterialId = historyMaterialId;
    }

    @Override
    public String toString() {
        return "HistoryMatDetailModel{" + "id=" + id + ", materialId=" + materialId + ", materialKey=" + materialKey + ", materialName=" + materialName + ", materialUnit=" + materialUnit + ", materialRemain=" + materialRemain + ", historyMaterialId=" + historyMaterialId + '}';
    }

    

    public static HistoryMatDetailModel fromRS(ResultSet rs) {
        HistoryMatDetailModel hisMatDetail = new HistoryMatDetailModel();
        try {
            hisMatDetail.setId(rs.getInt("history_material_detail_id"));
            hisMatDetail.setMaterialId(rs.getInt("material_id"));
            hisMatDetail.setMaterialKey(rs.getString("material_key"));
            hisMatDetail.setMaterialName(rs.getString("material_name"));
            hisMatDetail.setMaterialUnit(rs.getString("material_unit"));
            hisMatDetail.setMaterialRemain(rs.getInt("material_remain"));
            hisMatDetail.setHistoryMaterialId(rs.getInt("history_material_id"));
        } catch (SQLException ex) {
            Logger.getLogger(HistoryMatDetailModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return hisMatDetail;
    }
}
