/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.model;

import com.nawapat.dcoffeeproject.dao.UserDao;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class HistoryMatModel {

    private int id;
    private LocalDate createdDate;
    private int userId;
    private UserModel user;
    private ArrayList<HistoryMatDetailModel> hisMatDetails = new ArrayList<HistoryMatDetailModel>();

    public HistoryMatModel(int id, LocalDate createdDate, int userId) {
        this.id = id;
        this.createdDate = LocalDate.parse(createdDate.toString());
        this.userId = userId;
    }

    public HistoryMatModel(LocalDate createdDate, int userId) {
        this.id = id - 1;
        this.createdDate = createdDate;
        this.userId = userId;
    }

    public HistoryMatModel(int userId) {
        this.id = id - 1;
        this.createdDate = null;
        this.userId = userId;
    }

    public HistoryMatModel() {
        this.id = -1;
        this.createdDate = null;
        this.userId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
        this.userId = user.getId();
    }

    public ArrayList<HistoryMatDetailModel> getHisMatDetail() {
        return hisMatDetails;
    }

    public void setHisMatDetail(ArrayList<HistoryMatDetailModel> hisMatDetail) {
        this.hisMatDetails = hisMatDetail;
    }

    @Override
    public String toString() {
        return "HistoryMatModel{" + "id=" + id + ", createdDate=" + createdDate + ", userId=" + userId + ", user=" + user + ", hisMatDetail=" + hisMatDetails + '}';
    }

    public void addHisMatDetail(HistoryMatDetailModel hisMatDetail) {
        hisMatDetails.add(hisMatDetail);
    }

    public void addHisMatDetail(MaterialModel material) {
        HistoryMatDetailModel md = new HistoryMatDetailModel(material.getId(), material.getKey(), material.getName(), material.getUnit(),
                material.getRemain(), -1);
        hisMatDetails.add(md);
    }

    public void delHisMatDetail(HistoryMatDetailModel hisMatDetail) {
        hisMatDetails.remove(hisMatDetail);
    }

    public static HistoryMatModel fromRS(ResultSet rs) {
        HistoryMatModel hisMat = new HistoryMatModel();
        try {
            hisMat.setId(rs.getInt("history_material_id"));
            hisMat.setCreatedDate(rs.getDate("history_material_date").toLocalDate());
            hisMat.setUserId(rs.getInt("user_id"));

            //Population
            UserDao userDao = new UserDao();
            UserModel user = userDao.get(hisMat.getUserId());
            hisMat.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(HistoryMatModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return hisMat;
    }
}
