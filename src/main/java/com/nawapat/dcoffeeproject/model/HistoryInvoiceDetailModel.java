/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class HistoryInvoiceDetailModel {

    private int id;
    private int materialId;
    private String materialKey;
    private String materialName;
    private String materialUnit;
    private float materialPrice;
    private int materialQty;
    private int materialTotal;
    private String materialCompany;
    private int materialInvoiceId;

    public HistoryInvoiceDetailModel(int id, int materialId, String materialKey, String materialName, String materialUnit, float materialPrice, int materialQty, int materialTotal, String materialCompany, int materialInvoiceId) {
        this.id = id;
        this.materialId = materialId;
        this.materialKey = materialKey;
        this.materialName = materialName;
        this.materialUnit = materialUnit;
        this.materialPrice = materialPrice;
        this.materialQty = materialQty;
        this.materialTotal = materialTotal;
        this.materialCompany = materialCompany;
        this.materialInvoiceId = materialInvoiceId;
    }

    public HistoryInvoiceDetailModel(int materialId, String materialKey, String materialName, String materialUnit, float materialPrice, int materialQty, int materialTotal, String materialCompany, int materialInvoiceId) {
        this.id = -1;
        this.materialId = materialId;
        this.materialKey = materialKey;
        this.materialName = materialName;
        this.materialUnit = materialUnit;
        this.materialPrice = materialPrice;
        this.materialQty = materialQty;
        this.materialTotal = materialTotal;
        this.materialCompany = materialCompany;
        this.materialInvoiceId = materialInvoiceId;
    }

    public HistoryInvoiceDetailModel() {
        this.id = -1;
        this.materialId = 0;
        this.materialKey = "";
        this.materialName = "";
        this.materialUnit = "";
        this.materialPrice = 0;
        this.materialQty = 0;
        this.materialTotal = 0;
        this.materialCompany = "";
        this.materialInvoiceId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public String getMaterialKey() {
        return materialKey;
    }

    public void setMaterialKey(String materialKey) {
        this.materialKey = materialKey;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getMaterialUnit() {
        return materialUnit;
    }

    public void setMaterialUnit(String materialUnit) {
        this.materialUnit = materialUnit;
    }

    public float getMaterialPrice() {
        return materialPrice;
    }

    public void setMaterialPrice(float materialPrice) {
        this.materialPrice = materialPrice;
    }

    public int getMaterialQty() {
        return materialQty;
    }

    public void setMaterialQty(int materialQty) {
        this.materialQty = materialQty;
        materialTotal = (int) (materialQty * materialPrice);
    }

    public int getMaterialTotal() {
        return materialTotal;
    }

    public void setMaterialTotal(int materialTotal) {
        this.materialTotal = materialTotal;
    }

    public String getMaterialCompany() {
        return materialCompany;
    }

    public void setMaterialCompany(String materialCompany) {
        this.materialCompany = materialCompany;
    }

    public int getMaterialInvoiceId() {
        return materialInvoiceId;
    }

    public void setMaterialInvoiceId(int materialInvoiceId) {
        this.materialInvoiceId = materialInvoiceId;
    }

    @Override
    public String toString() {
        return "HistoryInvoiceDetailModel{" + "id=" + id + ", materialId=" + materialId + ", materialKey=" + materialKey + ", materialName=" + materialName + ", materialUnit=" + materialUnit + ", materialPrice=" + materialPrice + ", materialQty=" + materialQty + ", materialTotal=" + materialTotal + ", materialCompany=" + materialCompany + ", materialInvoiceId=" + materialInvoiceId + '}';
    }

    public static HistoryInvoiceDetailModel fromRS(ResultSet rs) {
        HistoryInvoiceDetailModel hisInvoiceDetail = new HistoryInvoiceDetailModel();
        try {
            hisInvoiceDetail.setId(rs.getInt("material_invoice_detail_id"));
            hisInvoiceDetail.setMaterialId(rs.getInt("material_id"));
            hisInvoiceDetail.setMaterialKey(rs.getString("material_key"));
            hisInvoiceDetail.setMaterialName(rs.getString("material_name"));
            hisInvoiceDetail.setMaterialUnit(rs.getString("material_unit"));
            hisInvoiceDetail.setMaterialPrice(rs.getFloat("material_price"));
            hisInvoiceDetail.setMaterialQty(rs.getInt("material_qty"));
            hisInvoiceDetail.setMaterialTotal(rs.getInt("material_total"));
            hisInvoiceDetail.setMaterialCompany(rs.getString("material_company"));
            hisInvoiceDetail.setMaterialInvoiceId(rs.getInt("material_invoice_id"));
            
        } catch (SQLException ex) {
            Logger.getLogger(HistoryInvoiceDetailModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return hisInvoiceDetail;
    }
}
