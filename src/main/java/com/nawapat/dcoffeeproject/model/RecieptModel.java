/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.model;

import com.nawapat.dcoffeeproject.dao.CustomerDao;
import com.nawapat.dcoffeeproject.dao.UserDao;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class RecieptModel {

    private int id;
    private Date createdDate;
    private float total;
    private float cash;
    private int totalQty;
    private int userId;
    private int customerId;
    private UserModel user;
    private CustomerModel customer;
    private ArrayList<RecieptDetailModel> recieptDetails = new ArrayList<RecieptDetailModel>();

    public RecieptModel(int id, Date createdDate, float total, float cash, int totalQty, int userId, int customerId) {
        this.id = id;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
    }

    public RecieptModel(Date createdDate, float total, float cash, int totalQty, int userId, int customerId) {
        this.id = -1;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
    }

    public RecieptModel(float total, float cash, int totalQty, int userId, int customerId) {
        this.id = -1;
        this.createdDate = null;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
    }

    public RecieptModel(float cash, int userId, int customerId) {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.cash = cash;
        this.totalQty = 0;
        this.userId = userId;
        this.customerId = customerId;
    }

    public RecieptModel() {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.cash = 0;
        this.totalQty = 0;
        this.userId = 0;
        this.customerId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
        this.userId = user.getId();
    }

    public CustomerModel getCustomer() {
        return customer;
    }

//    public void setCustomer(CustomerModel customer) {
//        this.customer = customer;
//        this.customerId = customer.getId();
//    }

    public ArrayList<RecieptDetailModel> getRecieptDetails() {
        return recieptDetails;
    }

    public void setRecieptDetails(ArrayList recieptDetails) {
        this.recieptDetails = recieptDetails;
    }

    @Override
    public String toString() {
        return "Reciept{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", cash=" + cash + 
                ", totalQty=" + totalQty + ", userId=" + userId + ", customerId=" + customerId + ", user=" + user + ", customer=" + customer + 
                ", recieptDetails=" + recieptDetails + '}';
    }

    public void addRecieptDetail(RecieptDetailModel recieptDetail) {
        recieptDetails.add(recieptDetail);
        calculateTotal();
    }
    
    public void addRecieptDetail(ProductModel product, int qty) {
        RecieptDetailModel rd = new RecieptDetailModel(product.getId(), product.getName(), product.getPrice(),
                product.getSize(), product.getSweetLevel() , qty, qty * product.getPrice(), -1);
        recieptDetails.add(rd);
        calculateTotal();
    }

    public void addRecieptDetail(ProductModel product, String productSize, String productSweetLevel, int qty) {
        RecieptDetailModel rd = new RecieptDetailModel(product.getId(), product.getName(), product.getPrice(),
                productSize, productSweetLevel , qty, qty * product.getPrice(), -1);
        recieptDetails.add(rd);
        calculateTotal();
    }

    public void delRecieptDetail(RecieptDetailModel recieptDetail) {
        recieptDetails.remove(recieptDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (RecieptDetailModel rd : recieptDetails) {
            total += rd.getTotalPrice();
            totalQty += rd.getQty();
        }
        this.totalQty = totalQty;
        this.total = total;
    }

    public static RecieptModel fromRS(ResultSet rs) {
        RecieptModel reciept = new RecieptModel();
        try {
            reciept.setId(rs.getInt("reciept_id"));
            reciept.setCreatedDate(rs.getDate("reciept_date"));
            reciept.setTotal(rs.getFloat("total"));
            reciept.setCash(rs.getFloat("cash"));
            reciept.setTotalQty(rs.getInt("total_qty"));
            reciept.setUserId(rs.getInt("user_id"));
            reciept.setCustomerId(rs.getInt("customer_id"));
            //Population
            CustomerDao customerDao = new CustomerDao();
            UserDao userDao = new UserDao();
            CustomerModel customer = customerDao.get(reciept.getCustomerId());
            UserModel user = userDao.get(reciept.getUserId());
//            reciept.setCustomer(customer);
            reciept.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(RecieptModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }
}
