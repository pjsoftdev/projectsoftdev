/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.model;

import com.nawapat.dcoffeeproject.dao.EmployeeDao;
import com.nawapat.dcoffeeproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class PaymentModel {

    private int id;
    private Date date;
    private float total;
    private int employeeId;
    private EmployeeModel employee;

    public PaymentModel(int id, Date date, float total, int employeeId) {
        this.id = id;
        this.date = date;
        this.total = total;
        this.employeeId = employeeId;
    }

    public PaymentModel(Date date, float total, int employeeId) {
        this.id = -1;
        this.date = date;
        this.total = total;
        this.employeeId = employeeId;
    }

    public PaymentModel() {
        this.id = -1;
        this.date = null;
        this.total = 0;
        this.employeeId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }
    
    public EmployeeModel getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeModel employee ) {
        this.employee = employee;
        this.employeeId = employee.getId();
    }

    @Override
    public String toString() {
        return "Payment{" + "id=" + id + ", date=" + date + ", total=" + total + ", employeeId=" + employeeId + '}';
    }

    public static PaymentModel fromRS(ResultSet rs) {
        PaymentModel payment = new PaymentModel();
        try {
            payment.setId(rs.getInt("payment_id"));
            payment.setDate(rs.getTimestamp("payment_date"));
            payment.setTotal(rs.getFloat("payment_total"));
            payment.setEmployeeId(rs.getInt("employee_id"));
            
            EmployeeDao employeeDao = new EmployeeDao();
            EmployeeModel employee = employeeDao.get(payment.getEmployeeId());
            payment.setEmployee(employee);
            
            
        } catch (SQLException ex) {
            Logger.getLogger(PaymentModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return payment;
    }

}
