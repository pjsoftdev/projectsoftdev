/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.service;

import com.nawapat.dcoffeeproject.dao.CustomerDao;
import com.nawapat.dcoffeeproject.model.CustomerModel;
import java.util.List;

/**
 *
 * @author asus
 */
public class CustomerService {
    public static CustomerModel currentCustomer;
    
    public CustomerModel getCusById() {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.get(1);
    }
    
    public CustomerModel getCustomersIdByName(String name) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.getByNameSearchId(name);
    }
    
    public List<CustomerModel> getCustomers() {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.getAll(" customer_key asc");
    }
    
    public List<CustomerModel> getCustomersByPhone(String search) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.getByPhone(search);
    }

    public CustomerModel addNew(CustomerModel editedCustomer) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.save(editedCustomer);
    }

    public CustomerModel update(CustomerModel editedCustomer) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.update(editedCustomer);
    }

    public int delete(CustomerModel editedCustomer) {
        CustomerDao cusDao = new CustomerDao();
        return cusDao.delete(editedCustomer);
    }
}
