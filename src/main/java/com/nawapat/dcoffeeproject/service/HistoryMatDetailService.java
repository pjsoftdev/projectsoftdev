/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.service;

import com.nawapat.dcoffeeproject.dao.HistoryMatDao;
import com.nawapat.dcoffeeproject.dao.HistoryMatDetailDao;
import com.nawapat.dcoffeeproject.model.HistoryMatDetailModel;
import com.nawapat.dcoffeeproject.model.HistoryMatModel;
import java.util.List;

/**
 *
 * @author asus
 */
public class HistoryMatDetailService {
    
    public HistoryMatDetailModel getById(int id) {
        HistoryMatDetailDao hismatDetailDao = new HistoryMatDetailDao();
        return hismatDetailDao.get(id);
    }

    public List<HistoryMatDetailModel> getHistoryMatDetails() {
        HistoryMatDetailDao hismatdetailDao = new HistoryMatDetailDao();
        return hismatdetailDao.getAll(" history_material_detail_id asc");
    }
    
    public List<HistoryMatDetailModel> getHistoryMatDetailsById(int id) {
        HistoryMatDetailDao hismatdetailDao = new HistoryMatDetailDao();
        return hismatdetailDao.getAllById(id);
    }
    
    public HistoryMatDetailModel addNew(HistoryMatDetailModel editedHistoryMatDetail) {
        HistoryMatDetailDao hismatdetailDao = new HistoryMatDetailDao();
        return hismatdetailDao.save(editedHistoryMatDetail);
    }

    public HistoryMatDetailModel update(HistoryMatDetailModel editedHistoryMatDetail) {
        HistoryMatDetailDao hismatdetailDao = new HistoryMatDetailDao();
        return hismatdetailDao.update(editedHistoryMatDetail);
    }

    public int delete(HistoryMatDetailModel editedHistoryMatDetail) {
        HistoryMatDetailDao hismatdetailDao = new HistoryMatDetailDao();
        return hismatdetailDao.delete(editedHistoryMatDetail);
    }
}
