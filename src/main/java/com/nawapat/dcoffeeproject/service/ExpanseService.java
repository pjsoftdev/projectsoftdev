/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.service;

import com.nawapat.dcoffeeproject.dao.ExpanseDao;
import com.nawapat.dcoffeeproject.model.ExpanseModel;
import java.util.List;

/**
 *
 * @author asus
 */
public class ExpanseService {

    public List<ExpanseModel> getExpanses() {
        ExpanseDao expDao = new ExpanseDao();
        return expDao.getAll(" expanse_id asc");
    }

    public ExpanseModel addNew(ExpanseModel editedExpanse) {
        ExpanseDao expDao = new ExpanseDao();
        return expDao.save(editedExpanse);
    }

    public ExpanseModel update(ExpanseModel editedExpanse) {
        ExpanseDao expDao = new ExpanseDao();
        return expDao.update(editedExpanse);
    }

    public int delete(ExpanseModel editedExpanse) {
        ExpanseDao expDao = new ExpanseDao();
        return expDao.delete(editedExpanse);
    }
    
}
