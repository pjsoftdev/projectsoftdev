/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.service;

import com.nawapat.dcoffeeproject.dao.UserDao;

import com.nawapat.dcoffeeproject.model.UserModel;
import java.util.List;

/**
 *
 * @author werapan
 */
public class UserService {
    public static UserModel currentUser;
    public UserModel login(String login, String password) {
        UserDao userDao = new UserDao();
        UserModel user = userDao.getByLogin(login);
        if(user != null && user.getPassword().equals(password)) {
            currentUser = user;
            return user;
        }
        return null;
    }

    public static UserModel getCurrentUser() {
        return currentUser;
    }    
    
    public List<UserModel> getUsers(){
        UserDao userDao = new UserDao();
        return userDao.getAll(" user_login asc");
    }

    public UserModel addNew(UserModel editedUser) {
        UserDao userDao = new UserDao();
        return userDao.save(editedUser);
    }

    public UserModel update(UserModel editedUser) {
        UserDao userDao = new UserDao();
        return userDao.update(editedUser);
    }

    public int delete(UserModel editedUser) {
        UserDao userDao = new UserDao();
        return userDao.delete(editedUser);
    }

    public boolean authenticateUser(String username, String password) {
        UserDao userDao = new UserDao();
    UserModel user = userDao.getByLogin(username);
    if (user != null && user.getPassword().equals(password)) {
        currentUser = user;
        return true;
    }
    return false;
    }
    
    
}
    

