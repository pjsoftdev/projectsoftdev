/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.service;

import com.nawapat.dcoffeeproject.dao.RecieptDao;
import com.nawapat.dcoffeeproject.dao.RecieptDetailDao;
import com.nawapat.dcoffeeproject.model.RecieptDetailModel;
import com.nawapat.dcoffeeproject.model.RecieptModel;
import java.util.List;

/**
 *
 * @author werapan
 */
public class RecieptService {
    public RecieptModel getById(int id) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.get(id);
    }
            
    public List<RecieptModel> getReciepts(){
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getAll(" reciept_id desc");
    }
    
    public List<RecieptModel> getRecieptsByDate(String begin, String end){
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getAllByDate(" reciept_id desc", begin, end);
    }

    public RecieptModel addNew(RecieptModel editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        RecieptModel reciept = recieptDao.save(editedReciept);
        for(RecieptDetailModel rd: editedReciept.getRecieptDetails()) {
            rd.setRecieptId(reciept.getId());
            recieptDetailDao.save(rd);
        }
        return reciept;
    }

    public RecieptModel update(RecieptModel editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.update(editedReciept);
    }

    public int delete(RecieptModel editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.delete(editedReciept);
    }
}
