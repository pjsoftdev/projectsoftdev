/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.service;

import com.nawapat.dcoffeeproject.dao.HistoryMatDetailDao;
import com.nawapat.dcoffeeproject.dao.RecieptDao;
import com.nawapat.dcoffeeproject.dao.RecieptDetailDao;
import com.nawapat.dcoffeeproject.model.HistoryMatDetailModel;
import com.nawapat.dcoffeeproject.model.RecieptDetailModel;
import com.nawapat.dcoffeeproject.model.RecieptModel;
import java.util.List;

/**
 *
 * @author werapan
 */
public class RecieptDetailService {
    public RecieptDetailModel getById(int id) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.get(id);
    }
            
    public List<RecieptDetailModel> getRecieptDetails(){
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.getAll(" reciept_detail_id asc");
    }
    
    public List<RecieptDetailModel> getRecieptDetailsById(int id) {
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        return recieptDetailDao.getAllById(id);
    }
    
}
