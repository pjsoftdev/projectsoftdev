/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.service;

import com.nawapat.dcoffeeproject.dao.SalesDao;
import com.nawapat.dcoffeeproject.model.SalesReport;
import java.util.List;

/**
 *
 * @author parametsmac
 */
public class SalesSevice {

    public List<SalesReport> getTopTenSalesByTotalPrice() {
        SalesDao salesDao = new SalesDao();
        return salesDao.getSalesByTotalQty(10);
    }
    

    public List<SalesReport> getTopTenSalesByTotalPriceMonth() {
        SalesDao salesDao = new SalesDao();
        return salesDao.getSalesByTotalQtyDay(10);
    }
}
