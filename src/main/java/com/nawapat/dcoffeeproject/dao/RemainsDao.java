/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.dao;

import com.nawapat.dcoffeeproject.helper.DatabaseHelper;
import com.nawapat.dcoffeeproject.model.RemainsReport;
import com.nawapat.dcoffeeproject.model.SalesReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author parametsmac
 */
public class RemainsDao implements Dao<Remains> {

    @Override
    public Remains get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Remains> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Remains save(Remains obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Remains update(Remains obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Remains obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Remains> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<RemainsReport> getRemainByTotalRemain(int limit) {
        ArrayList<RemainsReport> rlist = new ArrayList();
        String sql = """
                     SELECT material_id, material_name, material_remain , material_unit
                     FROM material 
                     ORDER BY material_remain ASC 
                     LIMIT ?;""";
        
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                RemainsReport obj = RemainsReport.fromRS(rs);
                rlist.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return rlist;
    }

//    public List<SalesReport> getArtistByTotalPrice(String begin, String end, int limit) {
//        ArrayList<SalesReport> list = new ArrayList();
//        String sql = """
//                     SELECT art.*, SUM(ini.Quantity) TotalQuantity, SUM(ini.UnitPrice*ini.Quantity) as TotalPrice FROM artists art
//                     INNER JOIN albums alb ON alb.ArtistId=art.ArtistId
//                     INNER JOIN tracks tra ON tra.AlbumId=alb.AlbumId
//                     INNER JOIN invoice_items ini ON ini.TrackId=tra.TrackId
//                     INNER JOIN invoices inv ON inv.InvoiceId=ini.InvoiceId 
//                     AND inv.InvoiceDate BETWEEN ? AND ?
//                     
//                     GROUP BY art.ArtistId
//                     ORDER BY TotalPrice DESC
//                     LIMIT ?""";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, begin);
//            stmt.setString(2, end);
//            stmt.setInt(3, limit);
//            ResultSet rs = stmt.executeQuery();
//
//            while (rs.next()) {
//                SalesReport obj = SalesReport.fromRS(rs);
//                list.add(obj);
//
//            }
//
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return list;
//    }
}
