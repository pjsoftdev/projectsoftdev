/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.dao;

import com.nawapat.dcoffeeproject.helper.DatabaseHelper;
import com.nawapat.dcoffeeproject.model.ExpanseModel;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asus
 */
public class ExpanseDao implements Dao<ExpanseModel> {

    @Override
    public ExpanseModel get(int id) {
        ExpanseModel expanse = null;
        String sql = "SELECT * FROM expanse WHERE expanse_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                expanse = ExpanseModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return expanse;
    }


    public List<ExpanseModel> getAll() {
        ArrayList<ExpanseModel> list = new ArrayList();
        String sql = "SELECT * FROM expanse";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ExpanseModel expanse = ExpanseModel.fromRS(rs);
                list.add(expanse);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<ExpanseModel> getAll(String where, String order) {
        ArrayList<ExpanseModel> list = new ArrayList();
        String sql = "SELECT * FROM expanse where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ExpanseModel expanse = ExpanseModel.fromRS(rs);
                list.add(expanse);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ExpanseModel> getAll(String order) {
        ArrayList<ExpanseModel> list = new ArrayList();
        String sql = "SELECT * FROM expanse  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ExpanseModel expanse = ExpanseModel.fromRS(rs);
                list.add(expanse);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ExpanseModel save(ExpanseModel obj) {

        String sql = "INSERT INTO expanse (bill_name, ft_unit , total_unit, total)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getBill_name());
            stmt.setFloat(2, obj.getFt_unit());
            stmt.setFloat(3, obj.getTotal_unit());
            stmt.setFloat(4, obj.getTotal());
            
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public ExpanseModel update(ExpanseModel obj) {
        String sql = "UPDATE expanse"
                + " SET bill_name = ?, ft_unit = ?, total_unit = ? , total = ?"
                + " WHERE expanse_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getBill_name());
            stmt.setFloat(2, obj.getFt_unit());
            stmt.setFloat(3, obj.getTotal_unit());
            stmt.setFloat(4, obj.getTotal());
            stmt.setInt(5, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ExpanseModel obj) {
        String sql = "DELETE FROM expanse WHERE expanse_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
