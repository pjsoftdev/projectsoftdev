/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.dao;

import com.nawapat.dcoffeeproject.helper.DatabaseHelper;
import com.nawapat.dcoffeeproject.model.HistoryInvoiceDetailModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asus
 */
public class HistoryInvoiceDetailDao implements Dao<HistoryInvoiceDetailModel> {

    @Override
    public HistoryInvoiceDetailModel get(int id) {
        HistoryInvoiceDetailModel hisInvoiceDetail = null;
        String sql = "SELECT * FROM material_invoice_detail WHERE material_invoice_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                hisInvoiceDetail = HistoryInvoiceDetailModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return hisInvoiceDetail;
    }

    public List<HistoryInvoiceDetailModel> getAll() {
        ArrayList<HistoryInvoiceDetailModel> list = new ArrayList();
        String sql = "SELECT * FROM material_invoice_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                HistoryInvoiceDetailModel hisInvoiceDetail = HistoryInvoiceDetailModel.fromRS(rs);
                list.add(hisInvoiceDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<HistoryInvoiceDetailModel> getAll(String where, String order) {
        ArrayList<HistoryInvoiceDetailModel> list = new ArrayList();
        String sql = "SELECT * FROM material_invoice_detail where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                HistoryInvoiceDetailModel hisInvoiceDetail = HistoryInvoiceDetailModel.fromRS(rs);
                list.add(hisInvoiceDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<HistoryInvoiceDetailModel> getAll(String order) {
        ArrayList<HistoryInvoiceDetailModel> list = new ArrayList();
        String sql = "SELECT * FROM material_invoice_detail  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                HistoryInvoiceDetailModel hisInvoiceDetail = HistoryInvoiceDetailModel.fromRS(rs);
                list.add(hisInvoiceDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<HistoryInvoiceDetailModel> getAllById(int id) {
        ArrayList<HistoryInvoiceDetailModel> list = new ArrayList();
        String sql = "SELECT * FROM material_invoice_detail WHERE material_invoice_id  = ? ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                HistoryInvoiceDetailModel hisInvoiceDetail = HistoryInvoiceDetailModel.fromRS(rs);
                list.add(hisInvoiceDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public HistoryInvoiceDetailModel save(HistoryInvoiceDetailModel obj) {
        String sql = "INSERT INTO material_invoice_detail (material_id, material_key, material_name, material_unit, "
                + "material_price, material_qty, material_total, material_company, material_invoice_id)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMaterialId());
            stmt.setString(2, obj.getMaterialKey());
            stmt.setString(3, obj.getMaterialName());
            stmt.setString(4, obj.getMaterialUnit());
            stmt.setFloat(5, obj.getMaterialPrice());
            stmt.setInt(6, obj.getMaterialQty());
            stmt.setInt(7, obj.getMaterialTotal());
            stmt.setString(8, obj.getMaterialCompany());
            stmt.setInt(9, obj.getMaterialInvoiceId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

            // Update material_remain
            String updateRemainSql = "UPDATE material SET material_remain = material_remain + ? WHERE material_id = ?";
            PreparedStatement updateRemainStmt = conn.prepareStatement(updateRemainSql);
            updateRemainStmt.setInt(1, obj.getMaterialQty());
            updateRemainStmt.setInt(2, obj.getMaterialId());
            updateRemainStmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public HistoryInvoiceDetailModel update(HistoryInvoiceDetailModel obj) {
        String sql = "UPDATE material_invoice_detail"
                + " SET material_id = ?, material_key = ?, material_name = ?, material_unit = ?, material_price = ?, "
                + "material_qty = ?, material_total = ?, material_company = ?, material_invoice_id = ?"
                + " WHERE material_invoice_detail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMaterialId());
            stmt.setString(2, obj.getMaterialKey());
            stmt.setString(3, obj.getMaterialName());
            stmt.setString(4, obj.getMaterialUnit());
            stmt.setFloat(5, obj.getMaterialPrice());
            stmt.setInt(6, obj.getMaterialQty());
            stmt.setInt(7, obj.getMaterialTotal());
            stmt.setString(8, obj.getMaterialCompany());
            stmt.setInt(9, obj.getMaterialInvoiceId());
            stmt.setInt(10, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(HistoryInvoiceDetailModel obj) {
        String sql = "DELETE FROM material_invoice_detail WHERE material_invoice_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
