/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.dao;

import com.nawapat.dcoffeeproject.helper.DatabaseHelper;
import com.nawapat.dcoffeeproject.model.HistoryInvoiceModel;
import com.nawapat.dcoffeeproject.model.HistoryMatModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asus
 */
public class HistoryInvoiceDao implements Dao<HistoryInvoiceModel> {

    @Override
    public HistoryInvoiceModel get(int id) {
        HistoryInvoiceModel hisInvoice = null;
        String sql = "SELECT * FROM material_invoice WHERE material_invoice_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                hisInvoice = HistoryInvoiceModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return hisInvoice;
    }

    public List<HistoryInvoiceModel> getAll() {
        ArrayList<HistoryInvoiceModel> list = new ArrayList();
        String sql = "SELECT * FROM material_invoice";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                HistoryInvoiceModel hisInvoice = HistoryInvoiceModel.fromRS(rs);
                list.add(hisInvoice);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<HistoryInvoiceModel> getAll(String where, String order) {
        ArrayList<HistoryInvoiceModel> list = new ArrayList();
        String sql = "SELECT * FROM material_invoice where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                HistoryInvoiceModel hisInvoice = HistoryInvoiceModel.fromRS(rs);
                list.add(hisInvoice);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<HistoryInvoiceModel> getAll(String order) {
        ArrayList<HistoryInvoiceModel> list = new ArrayList();
        String sql = "SELECT * FROM material_invoice  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                HistoryInvoiceModel hisInvoice = HistoryInvoiceModel.fromRS(rs);
                list.add(hisInvoice);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<HistoryInvoiceModel> getAllByDate(String begin, String end, String order) {
        ArrayList<HistoryInvoiceModel> list = new ArrayList<>();
        String sql = "SELECT * FROM material_invoice WHERE material_invoice_date BETWEEN ? AND ? ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, begin);
            pstmt.setString(2, end);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                HistoryInvoiceModel hisInvoice = HistoryInvoiceModel.fromRS(rs);
                list.add(hisInvoice);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public HistoryInvoiceModel save(HistoryInvoiceModel obj) {

        String sql = "INSERT INTO material_invoice (material_invoice_total, user_id)"
                + "VALUES(?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setInt(2, obj.getUserId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public HistoryInvoiceModel update(HistoryInvoiceModel obj) {
        String sql = "UPDATE material_invoice"
                + " SET material_invoice_total = ?, user_id = ?"
                + " WHERE material_invoice_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setInt(2, obj.getUserId());
            stmt.setInt(3, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(HistoryInvoiceModel obj) {
        String sql = "DELETE FROM material_invoice WHERE material_invoice_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
