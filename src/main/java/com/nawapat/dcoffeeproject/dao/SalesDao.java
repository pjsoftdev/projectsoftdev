/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.dao;

import com.nawapat.dcoffeeproject.helper.DatabaseHelper;
import com.nawapat.dcoffeeproject.model.SalesReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author parametsmac
 */
public class SalesDao implements Dao<Sales>{

    @Override
    public Sales get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Sales> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Sales save(Sales obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Sales update(Sales obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Sales obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Sales> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    public List<SalesReport> getSalesByTotalQty(int limit) {
        ArrayList<SalesReport> list = new ArrayList();
        String sql = "SELECT rd.product_id, rd.product_name, " +
            "SUM(rd.qty) as total_quantity, " +
            "SUM(rd.qty * p.product_price) as total_price " +
            "FROM reciept_detail rd " +
            "JOIN reciept r ON rd.reciept_id = r.reciept_id " +
            "JOIN product p ON rd.product_id = p.product_id " +
            "GROUP BY rd.product_id, rd.product_name " +
            "ORDER BY total_quantity DESC " +
            "LIMIT ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                SalesReport obj = SalesReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<SalesReport> getSalesByTotalQtyDay(int limit) {
        ArrayList<SalesReport> list = new ArrayList();
        String sql = "SELECT rd.product_id, rd.product_name, " +
            "SUM(rd.qty) as total_quantity, " +
            "SUM(rd.qty * p.product_price) as total_price " +
            "FROM reciept_detail rd " +
            "JOIN reciept r ON rd.reciept_id = r.reciept_id " +
            "JOIN product p ON rd.product_id = p.product_id " +
            "WHERE r.reciept_date BETWEEN '2023-11-01' AND '2023-11-30' " +
            "GROUP BY rd.product_id, rd.product_name " +
            "ORDER BY total_quantity DESC " +
            "LIMIT ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                SalesReport obj = SalesReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }


    
    public List<SalesReport> getArtistByTotalPrice(String begin, String end, int limit) {
        ArrayList<SalesReport> list = new ArrayList();
        String sql = """
                     SELECT art.*, SUM(ini.Quantity) TotalQuantity, SUM(ini.UnitPrice*ini.Quantity) as TotalPrice FROM artists art
                     INNER JOIN albums alb ON alb.ArtistId=art.ArtistId
                     INNER JOIN tracks tra ON tra.AlbumId=alb.AlbumId
                     INNER JOIN invoice_items ini ON ini.TrackId=tra.TrackId
                     INNER JOIN invoices inv ON inv.InvoiceId=ini.InvoiceId 
                     AND inv.InvoiceDate BETWEEN ? AND ?
                     
                     GROUP BY art.ArtistId
                     ORDER BY TotalPrice DESC
                     LIMIT ?""";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                SalesReport obj = SalesReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
}
