/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nawapat.dcoffeeproject.dao;

import com.nawapat.dcoffeeproject.helper.DatabaseHelper;
import com.nawapat.dcoffeeproject.model.CustomerModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asus
 */
public class CustomerDao implements Dao<CustomerModel> {

    @Override
    public CustomerModel get(int id) {
        CustomerModel customer = null;
        String sql = "SELECT * FROM customer WHERE customer_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                customer = CustomerModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return customer;
    }

    public CustomerModel getByNameSearchId(String name) {
        CustomerModel customer = null;
        String sql = "SELECT customer_id FROM customer WHERE customer_name = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                customer = CustomerModel.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return customer;
    }

    public List<CustomerModel> getByPhone(String phone) {
        ArrayList<CustomerModel> list = new ArrayList();
        String sql = "SELECT * FROM customer WHERE customer_phone= ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, phone);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                CustomerModel customer = CustomerModel.fromRS(rs);
                list.add(customer);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CustomerModel> getAll() {
        ArrayList<CustomerModel> list = new ArrayList();
        String sql = "SELECT * FROM customer";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CustomerModel customer = CustomerModel.fromRS(rs);
                list.add(customer);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CustomerModel> getAll(String where, String order) {
        ArrayList<CustomerModel> list = new ArrayList();
        String sql = "SELECT * FROM customer where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CustomerModel customer = CustomerModel.fromRS(rs);
                list.add(customer);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CustomerModel> getAll(String order) {
        ArrayList<CustomerModel> list = new ArrayList();
        String sql = "SELECT * FROM customer  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CustomerModel customer = CustomerModel.fromRS(rs);
                list.add(customer);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CustomerModel save(CustomerModel obj) {

        String sql = "INSERT INTO customer (customer_key, customer_name, customer_phone, customer_point)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getKey());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getPhone());
            stmt.setInt(4, obj.getPoint());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CustomerModel update(CustomerModel obj) {
        String sql = "UPDATE customer"
                + " SET customer_key = ?, customer_name = ?, customer_phone = ?, customer_point = ?"
                + " WHERE customer_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getKey());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getPhone());
            stmt.setInt(4, obj.getPoint());
            stmt.setInt(5, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CustomerModel obj) {
        String sql = "DELETE FROM customer WHERE customer_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
