/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.nawapat.dcoffeeproject.ui;

import com.nawapat.dcoffeeproject.model.MaterialModel;
import com.nawapat.dcoffeeproject.service.MaterialService;
import component.InputDialogMat;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.print.PrinterException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author asus
 */
public class MaterialPanel extends javax.swing.JPanel {

    private final MaterialService materialService;
    private List<MaterialModel> list;
    private MaterialModel editedMat;

    private final JScrollPane scrMain; //เชื่อมไป Main

    public MaterialPanel(JScrollPane scrMain) {
        initComponents();
        this.scrMain = scrMain;  //เชื่อมไป Main
        showDate(); //โชว์Date
        showTime(); //โชว์Time
        initImage();

        materialService = new MaterialService();
        list = materialService.getMaterials();
        tbMat.setRowHeight(100);
        tbMat.setModel(new AbstractTableModel() {
            String[] columnNames = {"Image", "Key", "Name", "Price", "Unit", "Minimum", "Remain"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 7;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    case 0:
                        return ImageIcon.class;
                    default:
                        return String.class;
                }
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                MaterialModel stock = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        ImageIcon icon = new ImageIcon("./stock" + stock.getId() + ".png");
                        Image image = icon.getImage();
                        int width = image.getWidth(null);
                        int height = image.getHeight(null);
                        Image newImage = image.getScaledInstance((int) (100 * ((float) width) / height), 100, Image.SCALE_SMOOTH);
                        icon.setImage(newImage);
                        return icon;
                    case 1:
                        return stock.getKey();
                    case 2:
                        return stock.getName();
                    case 3:
                        return stock.getPrice();
                    case 4:
                        return stock.getUnit();
                    case 5:
                        return stock.getMin();
                    case 6:
                        return stock.getRemain();
                    default:
                        return "Unknown";
                }
            }
        });
    }

    private void initImage() {
        ImageIcon icon = new ImageIcon("./material.png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) ((57 * width) / height), 57, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        lblImage.setIcon(icon);

        ImageIcon iconAdd = new ImageIcon("./IconAdd.png");
        Image imageAdd = iconAdd.getImage();
        Image newImageAdd = imageAdd.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
        iconAdd.setImage(newImageAdd);
        btnAdd.setIcon(iconAdd);

        ImageIcon iconEdit = new ImageIcon("./IconEdit.png");
        Image imageEdit = iconEdit.getImage();
        Image newImageEdit = imageEdit.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
        iconEdit.setImage(newImageEdit);
        btnEdit.setIcon(iconEdit);

        ImageIcon iconDel = new ImageIcon("./IconDel.png");
        Image imageDel = iconDel.getImage();
        Image newImageDel = imageDel.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
        iconDel.setImage(newImageDel);
        btnDel.setIcon(iconDel);

        ImageIcon iconhistorymaterial = new ImageIcon("./historymaterial.png");
        Image imagehistorymaterial = iconhistorymaterial.getImage();
        Image newImagehistorymaterial = imagehistorymaterial.getScaledInstance((int) ((17 * width) / height), 17, Image.SCALE_SMOOTH);
        iconhistorymaterial.setImage(newImagehistorymaterial);
        btnHistoryMat.setIcon(iconhistorymaterial);

        ImageIcon iconinvoice = new ImageIcon("./invoice.png");
        Image imageinvoice = iconinvoice.getImage();
        Image newImageinvoice = imageinvoice.getScaledInstance((int) ((17 * width) / height), 17, Image.SCALE_SMOOTH);
        iconinvoice.setImage(newImageinvoice);
        btnHsitoryBuy.setIcon(iconinvoice);

        ImageIcon iconmaterialorder = new ImageIcon("./materialorder.png");
        Image imagematerialorder = iconmaterialorder.getImage();
        Image newImagematerialorder = imagematerialorder.getScaledInstance((int) ((20 * width) / height), 20, Image.SCALE_SMOOTH);
        iconmaterialorder.setImage(newImagematerialorder);
        btnMatOr.setIcon(iconmaterialorder);

        ImageIcon iconPrint = new ImageIcon("./IconPrint.png");
        Image imagePrint = iconPrint.getImage();
        Image newImagePrint = imagePrint.getScaledInstance((int) ((20 * width) / height), 20, Image.SCALE_SMOOTH);
        iconPrint.setImage(newImagePrint);
        btnPrint.setIcon(iconPrint);
    }

    void showDate() { //โชว์Date
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
        Date d = new Date();
        Date.setText(s.format(d));
    }

    void showTime() { //โชว์Time
        new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss a");
                Date d = new Date();
                Time.setText(s.format(d));
            }
        }).start();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblMat = new javax.swing.JLabel();
        Date = new javax.swing.JLabel();
        Time = new javax.swing.JLabel();
        btnHsitoryBuy = new javax.swing.JButton();
        btnHistoryMat = new javax.swing.JButton();
        lblImage = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnAdd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDel = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbMat = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        btnPrint = new javax.swing.JButton();
        btnMatOr = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 243, 228));
        setForeground(new java.awt.Color(72, 52, 52));

        jPanel1.setBackground(new java.awt.Color(255, 243, 228));
        jPanel1.setPreferredSize(new java.awt.Dimension(364, 69));

        lblMat.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblMat.setForeground(new java.awt.Color(72, 52, 52));
        lblMat.setText("Material Management");

        Date.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Date.setForeground(new java.awt.Color(72, 52, 52));
        Date.setText("00/00/0000");

        Time.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Time.setForeground(new java.awt.Color(72, 52, 52));
        Time.setText("00.00.00 00");

        btnHsitoryBuy.setBackground(new java.awt.Color(183, 183, 162));
        btnHsitoryBuy.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnHsitoryBuy.setForeground(new java.awt.Color(72, 52, 52));
        btnHsitoryBuy.setText("History Buy");
        btnHsitoryBuy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHsitoryBuyActionPerformed(evt);
            }
        });

        btnHistoryMat.setBackground(new java.awt.Color(183, 183, 162));
        btnHistoryMat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnHistoryMat.setForeground(new java.awt.Color(72, 52, 52));
        btnHistoryMat.setText("History Material");
        btnHistoryMat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHistoryMatActionPerformed(evt);
            }
        });

        lblImage.setPreferredSize(new java.awt.Dimension(57, 57));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMat)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(Date)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Time)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnHsitoryBuy, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnHistoryMat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblMat)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Date)
                            .addComponent(Time)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnHistoryMat)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnHsitoryBuy)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(238, 214, 196));

        btnAdd.setBackground(new java.awt.Color(183, 183, 162));
        btnAdd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnAdd.setForeground(new java.awt.Color(72, 52, 52));
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnEdit.setBackground(new java.awt.Color(183, 183, 162));
        btnEdit.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnEdit.setForeground(new java.awt.Color(72, 52, 52));
        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDel.setBackground(new java.awt.Color(183, 183, 162));
        btnDel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnDel.setForeground(new java.awt.Color(72, 52, 52));
        btnDel.setText("Delete");
        btnDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAdd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEdit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDel)
                .addContainerGap(613, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnEdit)
                    .addComponent(btnDel))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(255, 243, 228));

        tbMat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbMat.setForeground(new java.awt.Color(72, 52, 52));
        tbMat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbMat);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(255, 243, 228));

        btnPrint.setBackground(new java.awt.Color(183, 183, 162));
        btnPrint.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnPrint.setForeground(new java.awt.Color(72, 52, 52));
        btnPrint.setText("Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        btnMatOr.setBackground(new java.awt.Color(183, 183, 162));
        btnMatOr.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnMatOr.setForeground(new java.awt.Color(72, 52, 52));
        btnMatOr.setText("Material Order");
        btnMatOr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMatOrActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnMatOr)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPrint)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPrint)
                    .addComponent(btnMatOr))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 847, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnHsitoryBuyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHsitoryBuyActionPerformed
//        scrMain.setViewportView(new HistoryPanel(scrMain)); //เชื่อมไป History
        scrMain.setViewportView(new HistoryInvoicePanel(scrMain)); //เชื่อมไป History
    }//GEN-LAST:event_btnHsitoryBuyActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        MessageFormat header = new MessageFormat("Material check document printing");

        MessageFormat footer = new MessageFormat("Monitor: ..................");

        try {
            tbMat.print(JTable.PrintMode.FIT_WIDTH, header, footer);
        } catch (PrinterException e) {
            JOptionPane.showMessageDialog(null, "Cannot be Print !" + e.getMessage());
        }

    }//GEN-LAST:event_btnPrintActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        editedMat = new MaterialModel();
        openDialog1();
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        int selectedIndex = tbMat.getSelectedRow();
        if (selectedIndex >= 0) {
            editedMat = list.get(selectedIndex);
            openDialog1();
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelActionPerformed
        int selectedIndex = tbMat.getSelectedRow();
        if (selectedIndex >= 0) {
            editedMat = list.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this,
                    "Do you want to Delete?", "Caution!!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                materialService.delete(editedMat);
                refreshTable();
                ImageIcon icon = new ImageIcon("./deleted.png");
                Image image = icon.getImage(); // 
                Image newimg = image.getScaledInstance(45, 45, java.awt.Image.SCALE_SMOOTH); // 
                icon = new ImageIcon(newimg); // 
                JOptionPane.showMessageDialog(tbMat, "Deleted successfully!", "Material Management", HEIGHT, icon);
            }
        }
    }//GEN-LAST:event_btnDelActionPerformed

    private void btnMatOrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMatOrActionPerformed
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        MaterialOrderDialog MatDialog = new MaterialOrderDialog(frame, true);
        MatDialog.setVisible(true);
        refreshTable();
    }//GEN-LAST:event_btnMatOrActionPerformed

    private void btnHistoryMatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHistoryMatActionPerformed
        scrMain.setViewportView(new HistoryMat(scrMain)); //เชื่อมไป History
    }//GEN-LAST:event_btnHistoryMatActionPerformed

    public JScrollPane getScrMain() { //เชื่อมไป Main
        return scrMain;
    }

    private void refreshTable() {
        list = materialService.getMaterials();
        tbMat.revalidate();
        tbMat.repaint();
    }

    private void openDialog1() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        InputDialogMat inputDialog = new InputDialogMat(frame, editedMat);
        inputDialog.setLocationRelativeTo(this);
        inputDialog.setVisible(true);
        inputDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }

        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Date;
    private javax.swing.JLabel Time;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDel;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnHistoryMat;
    private javax.swing.JButton btnHsitoryBuy;
    private javax.swing.JButton btnMatOr;
    private javax.swing.JButton btnPrint;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblImage;
    private javax.swing.JLabel lblMat;
    private javax.swing.JTable tbMat;
    // End of variables declaration//GEN-END:variables
}
