/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.nawapat.dcoffeeproject.ui;

import com.nawapat.dcoffeeproject.dao.HistoryMatDao;
import com.nawapat.dcoffeeproject.model.HistoryMatDetailModel;
import com.nawapat.dcoffeeproject.model.HistoryMatModel;
import com.nawapat.dcoffeeproject.service.HistoryMatDetailService;
import com.nawapat.dcoffeeproject.service.HistoryMatService;
import com.nawapat.dcoffeeproject.service.UserService;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

public class HistoryMat extends javax.swing.JPanel {

    private final JScrollPane scrMain;
    ArrayList<HistoryMatModel> list;
    HistoryMatDao hismatDao = new HistoryMatDao();
    HistoryMatService hisMatService = new HistoryMatService();
    UserService userService = new UserService();
    HistoryMatModel hisMat;
    HistoryMatDetailModel hisMatDetail;
    AbstractTableModel tbMatDetailModel;
    private final HistoryMatDetailService hisMatDetailService;
    private List<HistoryMatDetailModel> matlist;
    private UtilDateModel model1;
    private UtilDateModel model2;
//    private final AbstractTableModel tbMatDetailModl;
    private final AbstractTableModel tbMatModel;

    public HistoryMat(JScrollPane scrMain) {
        initComponents();
        this.scrMain = scrMain;
        initDatePicker();
        initImage();

        hismatDao = new HistoryMatDao();
        hisMatService = new HistoryMatService();
        userService = new UserService();
        hisMatDetailService = new HistoryMatDetailService();

        list = hisMatService.getHistoryMats();
        tbMat.setRowHeight(30);
        tbMatModel = new AbstractTableModel() {
            String[] header = {"ID", "Employee", "Date"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                HistoryMatModel hisMaterial = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return hisMaterial.getId();
                    case 1:
                        return hisMaterial.getUser().getEmployee().getName();
                    case 2:
                        return hisMaterial.getCreatedDate();
                    default:
                        return "";
                }
            }
        };
        tbMat.setModel(tbMatModel);

        matlist = hisMatDetailService.getHistoryMatDetails();
        tbMatDetail.setRowHeight(30);
        tbMatDetailModel = new AbstractTableModel() {
            String[] header = {"Key", "Name", "Remain", "Unit"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return matlist.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                HistoryMatDetailModel hisMatDetailModel = matlist.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return hisMatDetailModel.getMaterialKey();
                    case 1:
                        return hisMatDetailModel.getMaterialName();
                    case 2:
                        return hisMatDetailModel.getMaterialRemain();
                    case 3:
                        return hisMatDetailModel.getMaterialUnit();
                    default:
                        return " ";
                }
            }
        };
        showDate(); //โชว์Date
        showTime(); //โชว์Time
    }

    private void initImage() {
        ImageIcon icon = new ImageIcon("./historymaterial.png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) ((57 * width) / height), 57, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        lblImage.setIcon(icon);
        
        ImageIcon iconback = new ImageIcon("./leftarrow.png");
        Image imageback = iconback.getImage();
        Image newImageback = imageback.getScaledInstance((int) ((15 * width) / height), 15, Image.SCALE_SMOOTH);
        iconback.setImage(newImageback);
        btnBack.setIcon(iconback);
        
        ImageIcon iconAdd = new ImageIcon("./IconAdd.png");
        Image imageAdd = iconAdd.getImage();
        Image newImageAdd = imageAdd.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
        iconAdd.setImage(newImageAdd);
        btnAdd.setIcon(iconAdd);
        
        ImageIcon iconDel = new ImageIcon("./IconDel.png");
        Image imageDel = iconDel.getImage();
        Image newImageDel = imageDel.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
        iconDel.setImage(newImageDel);
        btnDel.setIcon(iconDel);
        
        ImageIcon iconProcess = new ImageIcon("./calendar.png");
        Image imageProcess = iconProcess.getImage();
        Image newImageProcess = imageProcess.getScaledInstance((int) ((25 * width) / height), 25, Image.SCALE_SMOOTH);
        iconProcess.setImage(newImageProcess);
        btnProcess.setIcon(iconProcess);
    }

    void showDate() { //โชว์Date
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date d = new java.util.Date();
        Date.setText(s.format(d));
    }

    void showTime() { //โชว์Time
        new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss a");
                java.util.Date d = new java.util.Date();
                Time.setText(s.format(d));
            }
        }).start();
    }

    private void initDatePicker() {
        model1 = new UtilDateModel();
        Properties p1 = new Properties();
        p1.put("text.today", "Today");
        p1.put("text.month", "Month");
        p1.put("text.year", "Year");
        JDatePanelImpl datePanel1 = new JDatePanelImpl(model1, p1);
        JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1, new DateLabelFormatter());
        pnlDatePicker1.add(datePicker1);
        model1.setSelected(true);

        model2 = new UtilDateModel();
        Properties p2 = new Properties();
        p2.put("text.today", "Today");
        p2.put("text.month", "Month");
        p2.put("text.year", "Year");
        JDatePanelImpl datePanel2 = new JDatePanelImpl(model2, p2);
        JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2, new DateLabelFormatter());
        pnlDatePicker2.add(datePicker2);
        model2.setSelected(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        lblMat = new javax.swing.JLabel();
        Date = new javax.swing.JLabel();
        Time = new javax.swing.JLabel();
        lblImage = new javax.swing.JLabel();
        pnlDatePicker1 = new javax.swing.JPanel();
        pnlDatePicker2 = new javax.swing.JPanel();
        btnProcess = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        btnAdd = new javax.swing.JButton();
        btnDel = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        lblMaterial = new javax.swing.JLabel();
        lblMaterial1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbMatDetail = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbMat = new javax.swing.JTable();
        btnBack = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(255, 243, 228));
        jPanel1.setForeground(new java.awt.Color(72, 52, 52));

        jPanel2.setBackground(new java.awt.Color(255, 243, 228));

        lblMat.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblMat.setForeground(new java.awt.Color(72, 52, 52));
        lblMat.setText("History Material");

        Date.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Date.setForeground(new java.awt.Color(72, 52, 52));
        Date.setText("00/00/0000");

        Time.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Time.setForeground(new java.awt.Color(72, 52, 52));
        Time.setText("00.00.00 00");

        pnlDatePicker1.setBackground(new java.awt.Color(255, 243, 228));
        pnlDatePicker1.setForeground(new java.awt.Color(72, 52, 52));

        pnlDatePicker2.setBackground(new java.awt.Color(255, 243, 228));
        pnlDatePicker2.setForeground(new java.awt.Color(72, 52, 52));

        btnProcess.setBackground(new java.awt.Color(183, 183, 162));
        btnProcess.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnProcess.setForeground(new java.awt.Color(72, 52, 52));
        btnProcess.setText("Process");
        btnProcess.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcessActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lblMat)
                        .addGap(0, 703, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(Date)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Time)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pnlDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlDatePicker2, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnProcess, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblMat)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(Date)
                                    .addComponent(Time)))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(pnlDatePicker2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnProcess, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(pnlDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(lblImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(238, 214, 196));

        btnAdd.setBackground(new java.awt.Color(183, 183, 162));
        btnAdd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnAdd.setForeground(new java.awt.Color(72, 52, 52));
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnDel.setBackground(new java.awt.Color(183, 183, 162));
        btnDel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnDel.setForeground(new java.awt.Color(72, 52, 52));
        btnDel.setText("Delete");
        btnDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAdd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnDel))
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(255, 243, 228));

        lblMaterial.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblMaterial.setForeground(new java.awt.Color(72, 52, 52));
        lblMaterial.setText("History Material");

        lblMaterial1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblMaterial1.setForeground(new java.awt.Color(72, 52, 52));
        lblMaterial1.setText("History Material Detail");

        tbMatDetail.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbMatDetail.setForeground(new java.awt.Color(72, 52, 52));
        tbMatDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Key", "Name", "Remain", "Unit"
            }
        ));
        jScrollPane2.setViewportView(tbMatDetail);

        tbMat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbMat.setForeground(new java.awt.Color(72, 52, 52));
        tbMat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "ID", "User", "Date"
            }
        ));
        tbMat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbMatMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbMat);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblMaterial)
                .addGap(326, 326, 326)
                .addComponent(lblMaterial1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 433, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMaterial)
                    .addComponent(lblMaterial1))
                .addContainerGap(530, Short.MAX_VALUE))
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addGap(54, 54, 54)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 441, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                            .addGap(53, 53, 53)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 485, Short.MAX_VALUE)))
                    .addContainerGap()))
        );

        btnBack.setBackground(new java.awt.Color(183, 183, 162));
        btnBack.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnBack.setForeground(new java.awt.Color(72, 52, 52));
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBack)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(btnBack)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 940, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 730, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, 0)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        scrMain.setViewportView(new AddHistoryMaterialPanel(scrMain));
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelActionPerformed
        int selectedIndex = tbMat.getSelectedRow();
        if (selectedIndex >= 0) {
            hisMat = list.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this,
                    "Do you want to Delete?", "Caution!!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                hisMatService.delete(hisMat);
            }
            refreshTable();
        }    }//GEN-LAST:event_btnDelActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        scrMain.setViewportView(new MaterialPanel(scrMain));
    }//GEN-LAST:event_btnBackActionPerformed

    private void tbMatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbMatMouseClicked
        int selectedIndex = tbMat.getSelectedRow();

        if (selectedIndex >= 0) {
            HistoryMatModel hsm = list.get(selectedIndex);

            matlist = hisMatDetailService.getHistoryMatDetailsById(hsm.getId());
            tbMatDetail.setModel(tbMatDetailModel);
            refreshHisMatTable(hsm.getId());
        }
    }//GEN-LAST:event_tbMatMouseClicked

    private void btnProcessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcessActionPerformed
        String pattern = "yyy-MM-dd";
        SimpleDateFormat formater = new SimpleDateFormat(pattern);
        System.out.println("" + formater.format(model1.getValue()) + " " + formater.format(model2.getValue()));
        String begin = formater.format(model1.getValue());
        String end = formater.format(model2.getValue());
        list = hisMatService.getHistoryMatsByDate(begin, end);
        tbMatModel.fireTableDataChanged();
    }//GEN-LAST:event_btnProcessActionPerformed

    private void refreshTable() {
        list = hisMatService.getHistoryMats();
        tbMat.revalidate();
        tbMat.repaint();
    }

    private void refreshHisMatTable(int id) {
        matlist = hisMatDetailService.getHistoryMatDetailsById(id);
        tbMatDetail.revalidate();
        tbMatDetail.repaint();
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Date;
    private javax.swing.JLabel Time;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnDel;
    private javax.swing.JButton btnProcess;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblImage;
    private javax.swing.JLabel lblMat;
    private javax.swing.JLabel lblMaterial;
    private javax.swing.JLabel lblMaterial1;
    private javax.swing.JPanel pnlDatePicker1;
    private javax.swing.JPanel pnlDatePicker2;
    private javax.swing.JTable tbMat;
    private javax.swing.JTable tbMatDetail;
    // End of variables declaration//GEN-END:variables

//    private void initTable() {
//        tbMat.setModel(new AbstractTableModel() {
//            String[] header = {"ID", "User", "Date"};
//
//            @Override
//            public Class<?> getColumnClass(int columnIndex) {
//                switch (columnIndex) {
//                    case 0:
//                        return ImageIcon.class;
//                    default:
//                        return String.class;
//                }
//            }
//
//            @Override
//            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
//                ArrayList<HistoryMatDetailModel> historyDetails = hisMat.getHisMatDetail();
//                HistoryMatDetailModel recieptDetail = historyDetails.get(rowIndex);
//                if (columnIndex == 2) {
//                    int qty = Integer.parseInt((String) aValue);
//                    if (qty < 1) {
//                        return;
//                    }
////                    refreshRec();
//
//                }
//
//            }
//
//            @Override
//            public boolean isCellEditable(int rowIndex, int columnIndex) {
//
//                switch (columnIndex) {
//                    case 2:
//                        return true;
//                    default:
//                        return false;
//                }
//            }
//
//            @Override
//            public String getColumnName(int column) {
//                return header[column]; // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
//            }
//
//            @Override
//            public int getRowCount() {
//                return his.size();
//            }
//
//            @Override
//            public int getColumnCount() {
//                return 4;
//            }
//
//            @Override
//            public Object getValueAt(int rowIndex, int columnIndex) {
//                Product product = products.get(rowIndex);
//                switch (columnIndex) {
//                    case 0:
//                        ImageIcon icon = new ImageIcon("./product" + product.getId() + ".png");
//                        Image image = icon.getImage();
//                        int width = image.getWidth(null);
//                        int height = image.getHeight(null);
//                        Image newImage = image.getScaledInstance((int) ((100.0 * width) / height), 100, Image.SCALE_SMOOTH);
//                        icon.setImage(newImage);
//                        return icon;
//                    case 1:
//                        return product.getId();
//                    case 2:
//                        return product.getName();
//                    case 3:
//                        return product.getPrice();
//                    default:
//                        return "";
//                }
//            }
//        });
//    }
}
