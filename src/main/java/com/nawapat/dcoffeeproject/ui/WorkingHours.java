/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.nawapat.dcoffeeproject.ui;

import com.nawapat.dcoffeeproject.model.CheckInCheckOutModel;
import com.nawapat.dcoffeeproject.model.EmployeeModel;
import com.nawapat.dcoffeeproject.model.PaymentModel;
import com.nawapat.dcoffeeproject.service.CheckInCheckOutService;
import com.nawapat.dcoffeeproject.service.EmployeeService;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import org.jdatepicker.DateModel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author growt
 */
public class WorkingHours extends javax.swing.JDialog {

    private PaymentModel editedPayment;
    private final CheckInCheckOutService checkincheckoutService;
    private List<CheckInCheckOutModel> list;
    private CheckInCheckOutModel editedCheck;
    private final EmployeeService employeeService;

    /**
     * Creates new form WorkingHours
     */
    public WorkingHours(java.awt.Frame parent, PaymentModel editedPayment) {
        super(parent, true);
        initComponents();
        
        this.editedPayment = editedPayment;
        checkincheckoutService = new CheckInCheckOutService();

        employeeService = new EmployeeService();

        list = checkincheckoutService.getCheckInCheckOuts();

        
        tblTime.setRowHeight(30);

        tblTime.setModel(new AbstractTableModel() {
            private Map<Integer, Integer> totalDurationById = calculateTotalDurationForIDs();
            private Map<Float, Float> totalById = calculateTotalForIDs();
            private List<CheckInCheckOutModel> uniqueEmployeeRecords = getUniqueEmployeeRecords();

            String[] columnName = {"Key", "Name", "Houry-Salary", "Total-Time", "Total", "Status"};

            @Override
            public String getColumnName(int column) {
                return columnName[column];
            }

            @Override
            public int getRowCount() {
                return uniqueEmployeeRecords.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckInCheckOutModel checkInOut = uniqueEmployeeRecords.get(rowIndex);

                switch (columnIndex) {
                    case 0:
                        return checkInOut.getUser().getEmployee().getKey();
                    case 1:
                        return checkInOut.getUser().getEmployee().getName();
                    case 2:
                        return checkInOut.getUser().getEmployee().getHoursalary();
                    case 3:
                        int id = checkInOut.getUserid();
                        int totalDuration = totalDurationById.getOrDefault(id, 0);
                        return totalDuration;
                    case 4:
                        int idTotal = checkInOut.getUserid();
                        float totals = totalById.getOrDefault((float) idTotal, 0f);
                        return totals;
                    case 5:
                        int userId = checkInOut.getUserid();
                        return checkStatusForUser(userId);
//                        return (checkInOut.getStatus() == 1) ? "Approve" : "Disapprove";
                       
                    default:
                        return "unknown";
                }
            }

            private Map<Integer, Integer> calculateTotalDurationForIDs() {
                Map<Integer, Integer> totalDurationById = new HashMap<>();
                for (CheckInCheckOutModel A : list) {
                    if (A.getStatus() == 0) {
                        int id = A.getUserid();
                        int duration = A.getDuration();

                        int currentDuration = totalDurationById.getOrDefault(id, 0);
                        totalDurationById.put(id, currentDuration + duration);
                    }
                }
                return totalDurationById;
            }

            private Map<Float, Float> calculateTotalForIDs() {
                Map<Float, Float> totalById = new HashMap<>();
                for (CheckInCheckOutModel A : list) {
                    if (A.getStatus() == 0) {
                        int id = A.getUserid();
                        float total = A.getUser().getEmployee().getHoursalary() * A.getDuration();

                        float currentTotal = totalById.getOrDefault((float) id, 0f);
                        totalById.put((float) id, currentTotal + total);
                    }
                }
                return totalById;
            }

            private List<CheckInCheckOutModel> getUniqueEmployeeRecords() {
                Map<Integer, CheckInCheckOutModel> uniqueEmployeesMap = new HashMap<>();
                for (CheckInCheckOutModel record : list) {
                    int userId = record.getUserid();
                    uniqueEmployeesMap.put(userId, record);
                }
                return new ArrayList<>(uniqueEmployeesMap.values());
            }
            
            public String checkStatusForUser(int userId) {
                for (CheckInCheckOutModel record : list) {
                    if (record.getUserid() == userId && record.getStatus() == 0) {
                        return "Disapprove";
                    }
                }
                return "Approve";
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        lblMat = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblTime = new javax.swing.JTable();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 243, 228));
        setForeground(new java.awt.Color(238, 214, 196));

        jPanel2.setBackground(new java.awt.Color(255, 243, 228));

        lblMat.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblMat.setForeground(new java.awt.Color(72, 52, 52));
        lblMat.setText("Working Hours");

        jButton2.setBackground(new java.awt.Color(183, 183, 162));
        jButton2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton2.setForeground(new java.awt.Color(72, 52, 52));
        jButton2.setText("Payroll");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblMat)
                .addContainerGap(651, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblMat)
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(238, 214, 196));

        tblTime.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblTime);

        jButton3.setBackground(new java.awt.Color(183, 183, 162));
        jButton3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton3.setForeground(new java.awt.Color(72, 52, 52));
        jButton3.setText("Payroll");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 781, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(347, 347, 347)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(348, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 457, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(223, 223, 223)
                    .addComponent(jButton3)
                    .addContainerGap(223, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        CheckInCheckOutModel editedCheck = new CheckInCheckOutModel();

    int selectedRow = tblTime.getSelectedRow();

    if (selectedRow != -1) {     
        String id = (String) tblTime.getValueAt(selectedRow, 0);

        List<CheckInCheckOutModel> checkincheckoutModels = checkincheckoutService.getAllById(id);

        for (CheckInCheckOutModel model : checkincheckoutModels) {
            if (model.getStatus() == 0) {
                editedCheck = model;
                openDialogPayroll(editedCheck);
                refreshTable();
                return;
            }
            // If any record is Disapprove, show the message
            if (model.getStatus() == 1) {
                JOptionPane.showMessageDialog(tblTime, "Nothing to Payroll");
                return;
            }
        }

        // If all records are Disapproved
        JOptionPane.showMessageDialog(tblTime, "Nothing to Payroll");
    }


    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton3ActionPerformed
    private void openDialogPayroll(CheckInCheckOutModel editedCheck) {
        // Getting the parent frame of the dialog
        JFrame parentFrame = (JFrame) SwingUtilities.getWindowAncestor(this);

        PayrollDialog payrollDialog = new PayrollDialog(parentFrame, editedCheck); // ส่งค่า editedCheck ไปยัง PayrollDialog
        payrollDialog.setLocationRelativeTo(this);
        payrollDialog.setVisible(true);

        payrollDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
        });
    }
    
    
    
    
    
   
    
    

    private void refreshTable() {
        list = checkincheckoutService.getCheckInCheckOuts();
        tblTime.revalidate();
        tblTime.repaint();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblMat;
    private javax.swing.JTable tblTime;
    // End of variables declaration//GEN-END:variables
}
