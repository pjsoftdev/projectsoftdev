/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.nawapat.dcoffeeproject.ui;

import com.nawapat.dcoffeeproject.dao.HistoryMatDao;
import com.nawapat.dcoffeeproject.dao.RecieptDao;
import com.nawapat.dcoffeeproject.model.HistoryMatDetailModel;
import com.nawapat.dcoffeeproject.model.HistoryMatModel;
import com.nawapat.dcoffeeproject.model.RecieptDetailModel;
import com.nawapat.dcoffeeproject.model.RecieptModel;
import com.nawapat.dcoffeeproject.service.HistoryMatDetailService;
import com.nawapat.dcoffeeproject.service.HistoryMatService;
import com.nawapat.dcoffeeproject.service.RecieptDetailService;
import com.nawapat.dcoffeeproject.service.RecieptService;
import com.nawapat.dcoffeeproject.service.UserService;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

public class OrderHistoryPanel extends javax.swing.JPanel {

    private final JScrollPane scrMain;
    UserService userService = new UserService();
    private final RecieptDao recieptDao;
    private final RecieptService recieptService;
    private final RecieptDetailService recieptDetailService;
    private List<RecieptModel> reclist;
    private List<RecieptDetailModel> recdetail;
    private final AbstractTableModel tbRecDetailModel;
    private UtilDateModel model1;
    private UtilDateModel model2;
    private final AbstractTableModel tbRecModel;

//    private final AbstractTableModel tbMatDetailModl;
    public OrderHistoryPanel(JScrollPane scrMain) {
        initComponents();
        this.scrMain = scrMain;
        initDatePicker();
        initImage();
        
        recieptDao = new RecieptDao();
        recieptService = new RecieptService();
        userService = new UserService();
        recieptDetailService = new RecieptDetailService();

        reclist = recieptService.getReciepts();
        tbRec.setRowHeight(30);
        tbRecModel = new AbstractTableModel() {
            String[] header = {"ID", "Date", "Employee", "Customer", "QTY", "Total"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return reclist.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                RecieptModel reciept = reclist.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return reciept.getId();
                    case 1:
                        return reciept.getCreatedDate();
                    case 2:
                        return reciept.getUser().getEmployee().getName();
                    case 3:
                        if (reciept.getCustomerId() == 0) {
                            return "-";
                        } else {
                            return "Member";
                        }
                    case 4:
                        return reciept.getTotalQty();
                    case 5:
                        return reciept.getTotal();
                    default:
                        return "";
                }
            }

        };
        tbRec.setModel(tbRecModel);

        recdetail = recieptDetailService.getRecieptDetails();
        tbRecDetail.setRowHeight(30);
        tbRecDetailModel = new AbstractTableModel() {
            String[] header = {"Product Name", "Size", "Sweet Level", "Price", "QTY", "Total Price"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return recdetail.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                RecieptDetailModel recieptDetailModel = recdetail.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return recieptDetailModel.getProductName();
                    case 1:
                        return recieptDetailModel.getProductSize();
                    case 2:
                        return recieptDetailModel.getProductSweetLevel();
                    case 3:
                        return recieptDetailModel.getProductPrice();
                    case 4:
                        return recieptDetailModel.getQty();
                    case 5:
                        return recieptDetailModel.getTotalPrice();
                    default:
                        return " ";
                }
            }
        };
        showDate(); //โชว์Date
        showTime(); //โชว์Time
    }

    private void initImage() {
        ImageIcon icon = new ImageIcon("./reciept.png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) ((57 * width) / height), 57, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        lblImage.setIcon(icon);
        
        ImageIcon iconProcess = new ImageIcon("./calendar.png");
        Image imageProcess = iconProcess.getImage();
        Image newImageProcess = imageProcess.getScaledInstance((int) ((25 * width) / height), 25, Image.SCALE_SMOOTH);
        iconProcess.setImage(newImageProcess);
        btnProcess.setIcon(iconProcess);
    }

    void showDate() { //โชว์Date
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date d = new java.util.Date();
        Date.setText(s.format(d));
    }

    void showTime() { //โชว์Time
        new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss a");
                java.util.Date d = new java.util.Date();
                Time.setText(s.format(d));
            }
        }).start();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        lblMat = new javax.swing.JLabel();
        Date = new javax.swing.JLabel();
        Time = new javax.swing.JLabel();
        lblImage = new javax.swing.JLabel();
        btnProcess = new javax.swing.JButton();
        pnlDatePicker2 = new javax.swing.JPanel();
        pnlDatePicker1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        lblMaterial = new javax.swing.JLabel();
        lblMaterial1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbRecDetail = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbRec = new javax.swing.JTable();

        jPanel1.setBackground(new java.awt.Color(255, 243, 228));
        jPanel1.setForeground(new java.awt.Color(72, 52, 52));

        jPanel2.setBackground(new java.awt.Color(255, 243, 228));

        lblMat.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblMat.setForeground(new java.awt.Color(72, 52, 52));
        lblMat.setText("Order History");

        Date.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Date.setForeground(new java.awt.Color(72, 52, 52));
        Date.setText("00/00/0000");

        Time.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Time.setForeground(new java.awt.Color(72, 52, 52));
        Time.setText("00.00.00 00");

        btnProcess.setBackground(new java.awt.Color(183, 183, 162));
        btnProcess.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnProcess.setForeground(new java.awt.Color(72, 52, 52));
        btnProcess.setText("Process");
        btnProcess.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcessActionPerformed(evt);
            }
        });

        pnlDatePicker2.setBackground(new java.awt.Color(255, 243, 228));
        pnlDatePicker2.setForeground(new java.awt.Color(72, 52, 52));

        pnlDatePicker1.setBackground(new java.awt.Color(255, 243, 228));
        pnlDatePicker1.setForeground(new java.awt.Color(72, 52, 52));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lblMat)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(Date)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Time)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pnlDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pnlDatePicker2, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnProcess, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblMat)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(Date)
                                    .addComponent(Time)))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(pnlDatePicker2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnProcess, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(pnlDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(lblImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(238, 214, 196));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 940, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 35, Short.MAX_VALUE)
        );

        jPanel4.setBackground(new java.awt.Color(255, 243, 228));

        lblMaterial.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblMaterial.setForeground(new java.awt.Color(72, 52, 52));
        lblMaterial.setText("Order History");

        lblMaterial1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblMaterial1.setForeground(new java.awt.Color(72, 52, 52));
        lblMaterial1.setText("Order History Detail");

        tbRecDetail.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbRecDetail.setForeground(new java.awt.Color(72, 52, 52));
        tbRecDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product Name", "Size", "Sweet level", "Price", "QTY", "Total Price"
            }
        ));
        jScrollPane2.setViewportView(tbRecDetail);

        tbRec.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbRec.setForeground(new java.awt.Color(72, 52, 52));
        tbRec.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "ID", "User", "Date"
            }
        ));
        tbRec.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbRecMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbRec);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblMaterial)
                .addGap(344, 344, 344)
                .addComponent(lblMaterial1)
                .addContainerGap(360, Short.MAX_VALUE))
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 433, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMaterial)
                    .addComponent(lblMaterial1))
                .addContainerGap(559, Short.MAX_VALUE))
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGap(54, 54, 54)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 536, Short.MAX_VALUE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 441, Short.MAX_VALUE))
                    .addContainerGap()))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 940, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 718, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(0, 0, 0)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tbRecMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbRecMouseClicked
        int selectedIndex = tbRec.getSelectedRow();

        if (selectedIndex >= 0) {
            RecieptModel rcm = reclist.get(selectedIndex);

            recdetail = recieptDetailService.getRecieptDetailsById(rcm.getId());
            tbRecDetail.setModel(tbRecDetailModel);
            refreshHisMatTable(rcm.getId());
        }
    }//GEN-LAST:event_tbRecMouseClicked

    private void btnProcessActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcessActionPerformed
        String pattern = "yyy-MM-dd";
        SimpleDateFormat formater = new SimpleDateFormat(pattern);
        System.out.println("" + formater.format(model1.getValue()) + " " + formater.format(model2.getValue()));
        String begin = formater.format(model1.getValue());
        String end = formater.format(model2.getValue());
        reclist = recieptService.getRecieptsByDate(begin, end);
        tbRecModel.fireTableDataChanged();
    }//GEN-LAST:event_btnProcessActionPerformed

    private void refreshTable() {
        reclist = recieptService.getReciepts();
        tbRec.revalidate();
        tbRec.repaint();
    }

    private void refreshHisMatTable(int id) {
        recdetail = recieptDetailService.getRecieptDetailsById(id);
        tbRecDetail.revalidate();
        tbRecDetail.repaint();
    }

    private void initDatePicker() {
        model1 = new UtilDateModel();
        Properties p1 = new Properties();
        p1.put("text.today", "Today");
        p1.put("text.month", "Month");
        p1.put("text.year", "Year");
        JDatePanelImpl datePanel1 = new JDatePanelImpl(model1, p1);
        JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1, new DateLabelFormatter());
        pnlDatePicker1.add(datePicker1);
        model1.setSelected(true);

        model2 = new UtilDateModel();
        Properties p2 = new Properties();
        p2.put("text.today", "Today");
        p2.put("text.month", "Month");
        p2.put("text.year", "Year");
        JDatePanelImpl datePanel2 = new JDatePanelImpl(model2, p2);
        JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2, new DateLabelFormatter());
        pnlDatePicker2.add(datePicker2);
        model2.setSelected(true);
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Date;
    private javax.swing.JLabel Time;
    private javax.swing.JButton btnProcess;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblImage;
    private javax.swing.JLabel lblMat;
    private javax.swing.JLabel lblMaterial;
    private javax.swing.JLabel lblMaterial1;
    private javax.swing.JPanel pnlDatePicker1;
    private javax.swing.JPanel pnlDatePicker2;
    private javax.swing.JTable tbRec;
    private javax.swing.JTable tbRecDetail;
    // End of variables declaration//GEN-END:variables

//    private void initTable() {
//        tbMat.setModel(new AbstractTableModel() {
//            String[] header = {"ID", "User", "Date"};
//
//            @Override
//            public Class<?> getColumnClass(int columnIndex) {
//                switch (columnIndex) {
//                    case 0:
//                        return ImageIcon.class;
//                    default:
//                        return String.class;
//                }
//            }
//
//            @Override
//            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
//                ArrayList<HistoryMatDetailModel> historyDetails = hisMat.getHisMatDetail();
//                HistoryMatDetailModel recieptDetail = historyDetails.get(rowIndex);
//                if (columnIndex == 2) {
//                    int qty = Integer.parseInt((String) aValue);
//                    if (qty < 1) {
//                        return;
//                    }
////                    refreshRec();
//
//                }
//
//            }
//
//            @Override
//            public boolean isCellEditable(int rowIndex, int columnIndex) {
//
//                switch (columnIndex) {
//                    case 2:
//                        return true;
//                    default:
//                        return false;
//                }
//            }
//
//            @Override
//            public String getColumnName(int column) {
//                return header[column]; // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
//            }
//
//            @Override
//            public int getRowCount() {
//                return his.size();
//            }
//
//            @Override
//            public int getColumnCount() {
//                return 4;
//            }
//
//            @Override
//            public Object getValueAt(int rowIndex, int columnIndex) {
//                Product product = products.get(rowIndex);
//                switch (columnIndex) {
//                    case 0:
//                        ImageIcon icon = new ImageIcon("./product" + product.getId() + ".png");
//                        Image image = icon.getImage();
//                        int width = image.getWidth(null);
//                        int height = image.getHeight(null);
//                        Image newImage = image.getScaledInstance((int) ((100.0 * width) / height), 100, Image.SCALE_SMOOTH);
//                        icon.setImage(newImage);
//                        return icon;
//                    case 1:
//                        return product.getId();
//                    case 2:
//                        return product.getName();
//                    case 3:
//                        return product.getPrice();
//                    default:
//                        return "";
//                }
//            }
//        });
//    }
}
