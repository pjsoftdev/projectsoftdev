/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.nawapat.dcoffeeproject.ui;

import com.nawapat.dcoffeeproject.model.CustomerModel;
import com.nawapat.dcoffeeproject.model.MaterialModel;
import com.nawapat.dcoffeeproject.model.ProductModel;
import com.nawapat.dcoffeeproject.service.CustomerService;
import com.nawapat.dcoffeeproject.service.ProductService;
import component.InputDialogMat;
import component.InputDialogProduct;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author kornn
 */
public class ProductPanel extends javax.swing.JPanel {

    private List<ProductModel> list;
    private ProductService productService;
    private ProductModel editedProduct;

    /**
     * Creates new form ProductPanel
     */
    public ProductPanel() {
        initComponents();
        productAll();
        showDate(); //โชว์Date
        showTime(); //โชว์Time
        initImage();
    }

    private void initImage() {
        ImageIcon icon = new ImageIcon("./iconproduct.png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance((int) ((57 * width) / height), 57, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        lblImage.setIcon(icon);

        ImageIcon iconAdd = new ImageIcon("./IconAdd.png");
        Image imageAdd = iconAdd.getImage();
        Image newImageAdd = imageAdd.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
        iconAdd.setImage(newImageAdd);
        btnAdd.setIcon(iconAdd);

        ImageIcon iconEdit = new ImageIcon("./IconEdit.png");
        Image imageEdit = iconEdit.getImage();
        Image newImageEdit = imageEdit.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
        iconEdit.setImage(newImageEdit);
        btnEdit.setIcon(iconEdit);

        ImageIcon iconDel = new ImageIcon("./IconDel.png");
        Image imageDel = iconDel.getImage();
        Image newImageDel = imageDel.getScaledInstance((int) ((13 * width) / height), 13, Image.SCALE_SMOOTH);
        iconDel.setImage(newImageDel);
        btnDelete.setIcon(iconDel);

        ImageIcon iconAll = new ImageIcon("./drinkcoffee.png");
        Image imageAll = iconAll.getImage();
        Image newImageAll = imageAll.getScaledInstance((int) ((20 * width) / height), 20, Image.SCALE_SMOOTH);
        iconAll.setImage(newImageAll);
        btnAll.setIcon(iconAll);

        ImageIcon iconCoffee = new ImageIcon("./coffee.png");
        Image imageCoffee = iconCoffee.getImage();
        Image newImageCoffee = imageCoffee.getScaledInstance((int) ((20 * width) / height), 20, Image.SCALE_SMOOTH);
        iconCoffee.setImage(newImageCoffee);
        btnCoffee.setIcon(iconCoffee);

        ImageIcon iconBread = new ImageIcon("./bread.png");
        Image imageBread = iconBread.getImage();
        Image newImageBread = imageBread.getScaledInstance((int) ((20 * width) / height), 20, Image.SCALE_SMOOTH);
        iconBread.setImage(newImageBread);
        btnSnack.setIcon(iconBread);
    }

    void showDate() { //โชว์Date
        SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
        Date d = new Date();
        Date.setText(s.format(d));
    }

    void showTime() { //โชว์Time
        new Timer(0, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss a");
                Date d = new Date();
                Time.setText(s.format(d));
            }
        }).start();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblProduct = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        btnAdd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnCoffee = new javax.swing.JButton();
        btnSnack = new javax.swing.JButton();
        btnAll = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        Date = new javax.swing.JLabel();
        Time = new javax.swing.JLabel();
        lblImage = new javax.swing.JLabel();

        setBackground(new java.awt.Color(238, 214, 196));

        tblProduct.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblProduct.setForeground(new java.awt.Color(72, 52, 52));
        tblProduct.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblProduct);

        jPanel1.setBackground(new java.awt.Color(255, 243, 228));
        jPanel1.setPreferredSize(new java.awt.Dimension(870, 87));

        btnAdd.setBackground(new java.awt.Color(183, 183, 162));
        btnAdd.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnAdd.setForeground(new java.awt.Color(72, 52, 52));
        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnEdit.setBackground(new java.awt.Color(183, 183, 162));
        btnEdit.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnEdit.setForeground(new java.awt.Color(72, 52, 52));
        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDelete.setBackground(new java.awt.Color(183, 183, 162));
        btnDelete.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnDelete.setForeground(new java.awt.Color(72, 52, 52));
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnCoffee.setBackground(new java.awt.Color(183, 183, 162));
        btnCoffee.setFont(new java.awt.Font("Leelawadee UI", 0, 14)); // NOI18N
        btnCoffee.setForeground(new java.awt.Color(72, 52, 52));
        btnCoffee.setText("Drink");
        btnCoffee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCoffeeActionPerformed(evt);
            }
        });

        btnSnack.setBackground(new java.awt.Color(183, 183, 162));
        btnSnack.setFont(new java.awt.Font("Leelawadee UI", 0, 14)); // NOI18N
        btnSnack.setForeground(new java.awt.Color(72, 52, 52));
        btnSnack.setText("Cake");
        btnSnack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSnackActionPerformed(evt);
            }
        });

        btnAll.setBackground(new java.awt.Color(183, 183, 162));
        btnAll.setFont(new java.awt.Font("Leelawadee UI", 0, 14)); // NOI18N
        btnAll.setForeground(new java.awt.Color(72, 52, 52));
        btnAll.setText("All");
        btnAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAllActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAdd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEdit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAll)
                .addGap(4, 4, 4)
                .addComponent(btnCoffee)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSnack)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEdit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCoffee)
                    .addComponent(btnSnack)
                    .addComponent(btnAll))
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(238, 214, 196));
        jPanel2.setPreferredSize(new java.awt.Dimension(0, 69));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(72, 52, 52));
        jLabel2.setText("Product Management");

        Date.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Date.setForeground(new java.awt.Color(72, 52, 52));
        Date.setText("00/00/0000");

        Time.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Time.setForeground(new java.awt.Color(72, 52, 52));
        Time.setText("00.00.00 00");

        lblImage.setPreferredSize(new java.awt.Dimension(57, 57));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(Date)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Time)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Date)
                            .addComponent(Time))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(lblImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 870, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 858, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 437, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCoffeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCoffeeActionPerformed
        productService = new ProductService();
        tblProduct.setRowHeight(100);
        list = productService.getProductsByCategory1();
        tblProduct.setModel(new AbstractTableModel() {
            String[] columnNames = {"Image", "Key", "Name", "Price"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    case 0:
                        return ImageIcon.class;
                    default:
                        return String.class;
                }
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ProductModel product = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        ImageIcon icon = new ImageIcon("./product" + product.getId() + ".png");
                        Image image = icon.getImage();
                        int width = image.getWidth(null);
                        int height = image.getHeight(null);
                        Image newImage = image.getScaledInstance((int) (100 * ((float) width) / height), 100, Image.SCALE_SMOOTH);
                        icon.setImage(newImage);
                        return icon;
                    case 1:
                        return product.getKey();
                    case 2:
                        return product.getName();
                    case 3:
                        return product.getPrice();
                    default:
                        return "Unknown";
                }
            }
        });
    }//GEN-LAST:event_btnCoffeeActionPerformed

    private void btnSnackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSnackActionPerformed
        productService = new ProductService();
        tblProduct.setRowHeight(100);
        list = productService.getProductsByCategory2();
        tblProduct.setModel(new AbstractTableModel() {
            String[] columnNames = {"Image", "Key", "Name", "Price"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    case 0:
                        return ImageIcon.class;
                    default:
                        return String.class;
                }
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ProductModel product = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        ImageIcon icon = new ImageIcon("./product" + product.getId() + ".png");
                        Image image = icon.getImage();
                        int width = image.getWidth(null);
                        int height = image.getHeight(null);
                        Image newImage = image.getScaledInstance((int) (100 * ((float) width) / height), 100, Image.SCALE_SMOOTH);
                        icon.setImage(newImage);
                        return icon;
                    case 1:
                        return product.getKey();
                    case 2:
                        return product.getName();
                    case 3:
                        return product.getPrice();
                    default:
                        return "Unknown";
                }
            }
        });
    }//GEN-LAST:event_btnSnackActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        editedProduct = new ProductModel();
        openDialog1();
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        int selectedIndex = tblProduct.getSelectedRow();
        if (selectedIndex >= 0) {
            editedProduct = list.get(selectedIndex);
            openDialog1();
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tblProduct.getSelectedRow();
        if (selectedIndex >= 0) {
            editedProduct = list.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this,
                    "Do you want to Delete?", "Caution!!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                productService.delete(editedProduct);
                refreshTable();
                ImageIcon icon = new ImageIcon("./deleted.png");
                Image image = icon.getImage(); // 
                Image newimg = image.getScaledInstance(45, 45, java.awt.Image.SCALE_SMOOTH); // 
                icon = new ImageIcon(newimg); // 
                JOptionPane.showMessageDialog(tblProduct, "Deleted successfully!", "Product Management", HEIGHT, icon);
            }
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAllActionPerformed
        productAll();
    }//GEN-LAST:event_btnAllActionPerformed

    private void refreshTable() {
        list = productService.getProducts();
        tblProduct.revalidate();
        tblProduct.repaint();
    }

//    private void enableForm(boolean status) {
//        if (status == false) {
//            txtKey.setText("");
//            txtName.setText("");
//            txtPrice.setText("");
//            cmbCategory.setSelectedIndex(0);
//        }
//        txtKey.setEnabled(status);
//        txtName.setEnabled(status);
//        txtPrice.setEnabled(status);
//        btnSave.setEnabled(status);
//        btnClear.setEnabled(status);
//        txtKey.requestFocus();
//        cmbCategory.setEnabled(status);
//    }
//    private void setFormToObject() {
//        editedProduct.setKey(txtKey.getText());
//        editedProduct.setName(txtName.getText());
//        int price = Integer.parseInt(txtPrice.getText());
//        editedProduct.setPrice(price);
//        editedProduct.setCategory(cmbCategory.getSelectedIndex());
//    }
//
//    private void setObjectToForm() {
//        txtKey.setText(editedProduct.getKey());
//        txtName.setText(editedProduct.getName());
//        txtPrice.setText(String.valueOf(editedProduct.getPrice()));
//        cmbCategory.setSelectedIndex(editedProduct.getCategory());
//    }
    private void productAll() {
        productService = new ProductService();
        list = productService.getProducts();
//        enableForm(false);
        tblProduct.setRowHeight(100);
        tblProduct.setModel(new AbstractTableModel() {
            String[] columnNames = {"Image", "Key", "Name", "Price", "Category"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                    case 0:
                        return ImageIcon.class;
                    default:
                        return String.class;
                }
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ProductModel product = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        ImageIcon icon = new ImageIcon("./product" + product.getId() + ".png");
                        Image image = icon.getImage();
                        int width = image.getWidth(null);
                        int height = image.getHeight(null);
                        Image newImage = image.getScaledInstance((int) (100 * ((float) width) / height), 100, Image.SCALE_SMOOTH);
                        icon.setImage(newImage);
                        return icon;
                    case 1:
                        return product.getKey();
                    case 2:
                        return product.getName();
                    case 3:
                        return product.getPrice();
                    case 4:
                        if (product.getCategory() == 1) {
                            return "Drink";
                        } else {
                            return "Cake";
                        }
                    default:
                        return "Unknown";
                }
            }
        });
    }

    private void openDialog1() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        InputDialogProduct inputDialog = new InputDialogProduct(frame, editedProduct);
        inputDialog.setLocationRelativeTo(this);
        inputDialog.setVisible(true);
        inputDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }

        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Date;
    private javax.swing.JLabel Time;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnAll;
    private javax.swing.JButton btnCoffee;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnSnack;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblImage;
    private javax.swing.JTable tblProduct;
    // End of variables declaration//GEN-END:variables
}
